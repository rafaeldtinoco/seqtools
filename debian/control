Source: seqtools
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libgtk2.0-dev,
               libsqlite3-dev,
               libcurl4-openssl-dev | libcurl4-dev,
               libjsoncpp-dev,
               d-shlibs
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/med-team/seqtools
Vcs-Git: https://salsa.debian.org/med-team/seqtools.git
Homepage: https://www.sanger.ac.uk/science/tools/seqtools
Rules-Requires-Root: no

Package: libgbtools0
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: library for visualising sequence alignments
 The SeqTools package contains three tools for visualising sequence
 alignments: Blixem, Dotter and Belvu.
 .
 This package contains the library all three tools are linked against.

Package: libgbtools-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libgbtools0 (= ${binary:Version}),
         ${devlibs:Depends},
         ${misc:Depends}
Description: library for visualising sequence alignments (devel)
 The SeqTools package contains three tools for visualising sequence
 alignments: Blixem, Dotter and Belvu.
 .
 This package contains the static library and header files.

Package: belvu
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Breaks: acedb-other-belvu (<< 4.9.39+dfsg.02-2)
Provides: acedb-other-belvu
Replaces: acedb-other-belvu (<< 4.9.39+dfsg.02-2)
Description: multiple sequence alignment viewer and phylogenetic tool
 Belvu is a multiple sequence alignment viewer and phylogenetic tool with
 an extensive set of user-configurable modes to color residues.
 .
  * View multiple sequence alignments.
  * Residues can be coloured by conservation, with user-configurable
    cutoffs and colours.
  * Residues can be coloured by residue type (user-configurable).
  * Colour schemes can be imported or exported.
  * Swissprot (or PIR) entries can be fetched by double clicking.
  * The position in the alignment can be easily tracked.
  * Manual deletion of rows and columns.
  * Automatic editing of rows and columns based on customisable criteria:
     - removal of all-gap columns;
     - removal of all gaps;
     - removal of redundant sequences;
     - removal of a column by a user-specified percentage of gaps;
     - filtering of sequences by percent identity;
     - removal of sequences by a user-specified percentage of gaps;
     - removal of partial sequences (those starting or ending with
       gaps); and
     - removal of columns by conservation (with user-specified
       upper/lower cutoffs).
  * The alignment can be saved in Stockholm, Selex, MSF or FASTA format.
  * Distance matrices between sequences can be generated using a variety
    of distance metrics.
  * Distance matrices can be imported or exported.
  * Phylogenetic trees can be constructed based on various distance-based
    tree reconstruction algorithms.
  * Trees can be saved in New Hampshire format.
  * Belvu can perform bootstrap phylogenetic reconstruction.

Package: blixem
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: interactive browser of sequence alignments
 Blixem is an interactive browser of sequence alignments that have been
 stacked up in a "master-slave" multiple alignment; it is not a 'true'
 multiple alignment but a 'one-to-many' alignment.
 .
  * Overview section showing the positions of genes and alignments around
    the alignment window
  * Detail section showing the actual alignment of protein or nucleotide
    sequences to the genomic DNA sequence.
  * View alignments against both strands of the reference sequence.
  * View sequences in nucleotide or protein mode; in protein mode, Blixem
    will display the three-frame translation of the reference sequence.
  * Residues are highlighted in different colours depending on whether
    they are an exact match, conserved substitution or mismatch.
  * Gapped alignments are supported, with insertions and deletions being
    highlighted in the match sequence.
  * Matches can be sorted and filtered.
  * SNPs and other variations can be highlighted in the reference
    sequence.
  * Poly(A) tails can be displayed and poly(A) signals highlighted in the
    reference sequence.

Package: dotter
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Breaks: acedb-other-dotter (<< 4.9.39+dfsg.02-2)
Provides: acedb-other-dotter
Replaces: acedb-other-dotter (<< 4.9.39+dfsg.02-2)
Description: detailed comparison of two genomic sequences
 Dotter is a graphical dot-matrix program for detailed comparison of two
 sequences.
 .
  * Every residue in one sequence is compared to every residue in the
    other, and a matrix of scores is calculated.
  * One sequence is plotted on the x-axis and the other on the y-axis.
  * Noise is filtered out so that alignments appear as diagonal lines.
  * Pairwise scores are averaged over a sliding window to make the score
    matrix more intelligible.
  * The averaged score matrix forms a three-dimensional landscape,
    with the two sequences in two dimensions and the height of the
    peaks in the third. This landscape is projected onto two
    dimensions using a grey-scale image - the darker grey of a peak,
    the higher the score is.
  * The contrast and threshold of the grey-scale image can be adjusted
    interactively, without having to recalculate the score matrix.
  * An Alignment Tool is provided to examine the sequence alignment that
    the grey-scale image represents.
  * Known high-scoring pairs can be loaded from a GFF file and overlaid
    onto the plot.
  * Gene models can be loaded from GFF and displayed alongside the
    relevant axis.
  * Compare a sequence against itself to find internal repeats.
  * Find overlaps between multiple sequences by making a dot-plot of all
    sequences versus themselves.
  * Run Dotter in batch mode to create large, time-consuming dot-plots as
    a background process.
